package com.kardashovdmitry.task1;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;

/**
 * Created by Dmitry on 12.09.2015.
 */
public class Task1Test {
    private WebDriver driver;
    private HomePage page;
    private Logger log = LogManager.getLogger(Task1Test.class);


    @BeforeClass
    public void setUP() {
        log.info("Create a new Driver");
        driver = new FirefoxDriver();
        page = new HomePage(driver);
        log.info("Go to http://www.tut.by/");
        driver.get("http://www.tut.by/");
    }


    @Test
    public void findFirstString() {
        page.findOfString(page.getConstant("firstfindstring"));
        page.createResultsList();
        System.out.println("Amount of results: " + page.getAmountOfResults());
        if (!page.getEqualsResult(page.getConstant("secondfindstring")))
            //If the search string on the page is not found: exception org.openqa.selenium.WebDriverException
            // throw new org.openqa.selenium.WebDriverException("String "+ page.getConstant("secondfindstring") +" not found");
            log.error("String '"+ page.getConstant("secondfindstring") +"' not found");
    }

    @AfterClass
    public void tearDown() {
        log.info("Quit driver");
        driver.quit();
    }
}
