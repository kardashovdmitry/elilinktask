package com.kardashovdmitry.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Date;

/**
 * Created by Dmitry on 16.09.2015.
 */
public class Task3Test {
    private WebDriver driver;
    private HomePage homePage;
    private TripSummaryPage tripSummaryPage;
    private TicketsSelectionPage ticketsSelectionPage;
    private PassengerInfoPage passengerInfoPage;
    private ReviewPage reviewPage;
    private Logger log = LogManager.getLogger(Task3Test.class);


    @BeforeClass
    public void setUp() {
        log.info("Create a new Driver");
        driver = new FirefoxDriver();
        homePage = new HomePage(driver);
        tripSummaryPage = new TripSummaryPage(driver);
        ticketsSelectionPage = new TicketsSelectionPage(driver);
        passengerInfoPage = new PassengerInfoPage(driver);
        reviewPage = new ReviewPage(driver);
        log.info("https://www.delta.com/");
        driver.get("https://www.delta.com/");
    }

    @Test
    public void flightBooking() {
        //Step 2
        homePage.clickElement(homePage.bookATrip)
                .clickElement(homePage.flight)
                .fillField(homePage.getConstant("from_field"), homePage.fromField)
                .fillField(homePage.getConstant("to_field"), homePage.toField)
                .fillField(homePage.generateDate(homePage.getConstant("depart_date")), homePage.departDate)
                .fillField(homePage.generateDate(homePage.getConstant("return_date")), homePage.returnDate)
                .clickElement(homePage.exactDatesButton)
                .clickElement(homePage.moneyButton)
                .clickfindFlightsButton();

        //Step 3
        ticketsSelectionPage.selectTicket()
                            .waitSelectTicket()
                            .selectTicket();

        //Step 4
        tripSummaryPage.clickElement(tripSummaryPage.continueButton);

        //Step 5
        passengerInfoPage.fillField(passengerInfoPage.getConstant("first_name"), passengerInfoPage.firstName)
                         .fillField(passengerInfoPage.getConstant("last_name"), passengerInfoPage.lastName)
                         .clickElement(passengerInfoPage.gender)
                         .clickElement(passengerInfoPage.male)
                         .clickElement(passengerInfoPage.month)
                         .clickElement(passengerInfoPage.testMonth)
                         .clickElement(passengerInfoPage.year)
                         .clickElement(passengerInfoPage.testYear)
                         .clickElement(passengerInfoPage.day)
                         .clickElement(passengerInfoPage.testDay)
                         .fillField(passengerInfoPage.getConstant("first_name"), passengerInfoPage.emergencyFirstName)
                         .fillField(passengerInfoPage.getConstant("last_name"), passengerInfoPage.emergencyLastName)
                         .fillField(passengerInfoPage.getConstant("phone_number"), passengerInfoPage.emergencyPhoneNumber)
                         .fillField(passengerInfoPage.getConstant("phone_number"), passengerInfoPage.contactPhoneNumber)
                         .fillField(passengerInfoPage.getConstant("email"), passengerInfoPage.contactEmail)
                         .fillField(passengerInfoPage.getConstant("email"), passengerInfoPage.confirmContactEmail)
                         .clickElement(passengerInfoPage.continueButton);
        //Step 6-7
        reviewPage.checkButtonIsActive(reviewPage.continueButton);

    }

    @AfterClass
    public void tearDown() {
        log.info("Quit driver");
        driver.quit();
    }
}
