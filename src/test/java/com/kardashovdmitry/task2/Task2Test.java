package com.kardashovdmitry.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Dmitry on 15.09.2015.
 */
public class Task2Test {
    private WebDriver driver;
    private LoginPage loginPage;
    private Logger log = LogManager.getLogger(Task2Test.class);


    @BeforeClass
    public void setUp() {
        log.info("Create a new Driver");
        driver = new FirefoxDriver();
        loginPage = new LoginPage(driver);
        log.info("Go to http://gmail.com");
        driver.get("http://gmail.com");
    }

    @Test
    public void signIn() {

        loginPage.clickSignIn()
                 .submitUserName(loginPage.getConstant("email"))                         //Sign in
                 .submitPassword(loginPage.getConstant("password"))
                 .clickInbox()                                                          //Go to links
                 .clickSentMail()
                 .clickMore()
                 .clickSpam()
                 .clickComposeButton()                                                  //Send mail
                 .fillRecipientsField(loginPage.getConstant("recipients_field_text"))
                 .fillSubjectField(loginPage.getConstant("subject_field_text"))
                 .fillMailField(loginPage.getConstant("mail_field_text"))
                 .clickSendButton()
                 .clickInbox()                                                          //Find of string
                 .findOfString(loginPage.getConstant("find_string"))
                 .compareNumberOfResults(loginPage.getConstant("expected_result"))
                 .logOut();                                                             //Log out

    }

    @AfterClass
    public void tearDown() {
        log.info("Quit driver");
        driver.quit();
    }
}
