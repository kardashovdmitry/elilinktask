package com.kardashovdmitry.task2;


import com.kardashovdmitry.ReadXMLFile;
import com.kardashovdmitry.WaitHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dmitry on 15.09.2015.
 */
public class HomePage {
    private WebDriver driver;
    private ReadXMLFile read;
    private String PATH = "src/main/resources/test2const.xml";
    private By inbox = By.xpath("//a[contains(@href,'mail.google.com/mail/#inbox')]");
    private By sentMail = By.xpath("//a[contains(@href,'https://mail.google.com/mail/#sent')]");
    private By more = By.xpath("//span[@class='CJ']");
    private By spam = By.xpath("//a[contains(@href,'https://mail.google.com/mail/#spam')]");
    private By logOut = By.xpath("//a[contains(@href,'accounts.google.com/SignOutOptions')]");
    private By exit = By.id("gb_71");
    private By searchField = By.xpath("//input[@id='gbqfq']");
    private By composeButton = By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']");
    private By recipientsField = By.xpath("//textarea[@class='vO']");
    private By subjectField = By.xpath("//input[@class='aoT']");
    private By mailField = By.xpath("//div[@class='Am Al editable LW-avf']");
    private By sendButton = By.xpath("//div[@class='T-I J-J5-Ji aoO T-I-atl L3']");
    private By searchButton = By.xpath("//button[@class='gbqfb']");
    private By listOfResultsLocator = By.xpath("//tr[@class='zA zE']");
    private Logger log = LogManager.getLogger(HomePage.class);

    public HomePage(WebDriver driver) {
        log.info("Create a new HomePage object");
        this.driver = driver;
    }

    public HomePage typeKeyword(String s) {
        log.info("Type " + s + " to search field");
        driver.findElement(searchField).sendKeys(s);
        return this;
    }

    public HomePage submitFindField() {
        log.info("Click search button");
        driver.findElement(searchButton).click();
        return this;
    }

    public HomePage findOfString(String s) {
        log.info("Find '" + s + "'");
        this.typeKeyword(s);
        this.submitFindField();
        return this;
    }

    public HomePage clickInbox() {
        log.info("Go to Inbox page ");
        WaitHelper.setWaitTime(100, inbox, driver);
        driver.findElement(inbox).click();
        if (!(new WebDriverWait(driver, 100)).until(ExpectedConditions.titleContains("Входящие"))){
            throw new NoSuchWindowException("Page 'Inbox' not found");
        }
        return this;
    }

    public HomePage clickSentMail() {
        log.info("Go to SentMail page ");
        WaitHelper.setWaitTime(50, sentMail, driver);
        driver.findElement(sentMail).click();
        if (!(new WebDriverWait(driver, 100)).until(ExpectedConditions.titleContains("Отправленные"))){
            throw new NoSuchWindowException("Page 'SentMail' not found");
        }
        return this;
    }

    public HomePage clickMore() {
        log.info("Click More button");
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(sentMail)).perform();
        WaitHelper.setWaitTime(50, more, driver);
        driver.findElement(more).click();
        return this;
    }

    public HomePage clickSpam(){
        log.info("Go to Spam page ");
        WaitHelper.setWaitTime(50, more, driver);
        driver.findElement(spam).click();
        if (!(new WebDriverWait(driver, 50)).until(ExpectedConditions.titleContains("Спам"))){
            throw new NoSuchWindowException("Page 'Spam' not found");
        }
        return this;
    }

    public void logOut() {
        log.info("Log out");
        driver.findElement(logOut).click();
        driver.findElement(exit).click();
    }

    public HomePage clickComposeButton() {
        log.info("Click Compose Button");
        WaitHelper.setWaitTime(50, composeButton, driver);
        driver.findElement(composeButton).click();
        return this;
    }

    public HomePage fillRecipientsField(String s) {
        log.info("Fill '" + s + "' to Recipients Field");
        WaitHelper.setWaitTime(50, recipientsField, driver);
        driver.findElement(recipientsField).sendKeys(s);
        return this;
    }

    public HomePage fillSubjectField(String s) {
        log.info("Fill '" + s + "' to Subject Field");
        WaitHelper.setWaitTime(50, subjectField, driver);
        driver.findElement(subjectField).sendKeys(s);
        return this;
    }

    public HomePage fillMailField(String s) {
        log.info("Fill '" + s + "' to Mail Field");
        WaitHelper.setWaitTime(50, mailField, driver);
        driver.findElement(mailField).sendKeys(s);
        return this;
    }

    public HomePage clickSendButton() {
        log.info("Click Send Button");
        WaitHelper.setWaitTime(50, sendButton, driver);
        driver.findElement(sendButton).click();
        return this;
    }
    //Return the number of results per page
    public int getAmountOfResults() {
        log.info("Execute 'getAmountOfResults' method");
        List<WebElement> elements = driver.findElements(listOfResultsLocator);
        Iterator<WebElement> itr = elements.iterator();
        int count = 0;
        while(itr.hasNext()) {
            WebElement element = itr.next();
            if(element.isEnabled()) {
                count++;
            }
        }
        return count;
    }
    //Compare the expected and actual results
    public HomePage compareNumberOfResults(String result) {
        log.info("Execute 'compareNumberOfResults' method");
        int expectedResult = Integer.parseInt(result);
        int actualResult = getAmountOfResults();
        if (actualResult == expectedResult) {
            System.out.println("Results equal: " + actualResult + " = " + expectedResult);
        } else {
            System.out.println("Results are not equal: " + actualResult + " = " + expectedResult);
        }
        return this;
    }

    public String getConstant(String constant) {
        log.info("Get '" + constant + "' const");
        read = new ReadXMLFile();
        return read.getConstant(constant, PATH);
    }

}
