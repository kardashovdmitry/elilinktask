package com.kardashovdmitry.task2;

import com.kardashovdmitry.ReadXMLFile;
import com.kardashovdmitry.WaitHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Dmitry on 15.09.2015.
 */
public class LoginPage {
    private WebDriver driver;
    private ReadXMLFile read;
    private String PATH = "src/main/resources/test2const.xml";
    private By signIn = By.id("gmail-sign-in");
    private By userName = By.id("Email");
    private By password = By.id("Passwd");
    private Logger log = LogManager.getLogger(LoginPage.class);

    public LoginPage(WebDriver driver) {
        log.info("Create a new LoginPage object");
        this.driver = driver;
    }

    public LoginPage typeUserName(String s) {
        log.info("Type " + s + " to User Name field");
        driver.findElement(userName).sendKeys(s);
        return this;
    }

    public LoginPage submitUserNameField() {
        log.info("Submit User Name field");
        driver.findElement(userName).submit();
        return this;
    }

    public LoginPage submitUserName(String strUserName) {
        typeUserName(strUserName);
        submitUserNameField();
        return this;
    }

    public LoginPage typePassword(String s) {
        log.info("Type " + s + " to Password field");
        driver.findElement(password).sendKeys(s);
        return this;
    }

    public LoginPage submitPasswordField() {
        log.info("Submit Password field");
        driver.findElement(password).submit();
        return this;
    }

    public HomePage submitPassword(String pass) {
        WaitHelper.setWaitTime(50, password, driver);
        typePassword(pass);
        submitPasswordField();
        return new HomePage(driver);
    }
    //Check to enter the page for the first time
    public LoginPage clickSignIn() {
        try {
            log.info("Click SignIn button");
            driver.findElement(signIn).click();
        } catch (Exception e) {
            return this;
        }
        return this;
    }

    public String getConstant(String constant) {
        log.info("Get '" + constant + "' const");
        read = new ReadXMLFile();
        return read.getConstant(constant, PATH);
    }
}
