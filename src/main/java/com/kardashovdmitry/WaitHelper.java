package com.kardashovdmitry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Dmitry on 21.09.2015.
 */
public class WaitHelper {
    //Wait for the desired item on the page
    public static void setWaitTime(int time,By locator, WebDriver driver) {
        WebElement dynamicElement = (new WebDriverWait(driver, time))
                .until(ExpectedConditions.presenceOfElementLocated(locator));

    }

}
