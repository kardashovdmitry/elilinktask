package com.kardashovdmitry.task1;

import com.kardashovdmitry.ReadXMLFile;
import com.kardashovdmitry.WaitHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;

import javax.xml.parsers.DocumentBuilderFactory;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.Document;

import org.w3c.dom.Element;

import org.w3c.dom.Node;

import org.w3c.dom.NodeList;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Dmitry on 12.09.2015.
 */
public class HomePage {
    private final WebDriver driver;
    private ReadXMLFile read;
    private String PATH = "src/main/resources/test1const.xml";
    private List<List<String>> allResults;
    private By searhField = By.id("search_from_str");
    private By searhButton = By.xpath("//input[@class='button big']");
    private By listOfResults = By.xpath("//li[@class='b-results__li']");
    private By equalsResults = By.xpath("//li[@class='b-results__li']//h3//a[2]");
    private By nextPageButton = By.xpath("//strong[text()='Следующая страница']");
    private Logger log = LogManager.getLogger(HomePage.class);


    public HomePage(WebDriver driver) {
        log.info("Create a new HomePage object");
        this.driver = driver;
    }

    public void typeKeyword(String s) {
        log.info("Type " + s + " to search field");
        driver.findElement(searhField).sendKeys(s);
    }

    public void submitFindField() {
        log.info("Click search button");
        driver.findElement(searhButton).click();
    }

    public void findOfString(String s) {
        this.typeKeyword(s);
        this.submitFindField();
    }

    public void clearFindField() {
        log.info("Execute 'clearFindField' method");
        driver.findElement(searhField).clear();
    }

    public List<List<String>> createResultsList(){
        log.info("Execute 'createResultsList' method");
        WebElement nextPageButtonElement = driver.findElement(nextPageButton);
        allResults = new ArrayList<List<String>>();
        int count = 0;
        //for all pages put: nextPageButtonElement.isDisplayed()
        while (count != 1) {
            List<String> listResultsEachPages = new ArrayList<String>();
            List<WebElement> elements = driver.findElements(equalsResults);
            Iterator<WebElement> itr = elements.iterator();
            while(itr.hasNext()) {
                WebElement element = itr.next();
                listResultsEachPages.add(element.getText());
            }
            allResults.add(listResultsEachPages);
            count ++;
        }
        return allResults;
    }
    //Finding the number of results per page
    public int getAmountOfResults() {
        log.info("Execute 'getAmountOfResults' method");
        Iterator<List<String>> itr = allResults.iterator();
        int amountResults = 0;
        while (itr.hasNext()){
            List<String> list = itr.next();
            amountResults += list.size();
        }
        return amountResults;
    }
    //Go through the pages of results
    public void clickNextPage() {
        log.info("Click 'nextPage' button");
        driver.findElement(nextPageButton).click();
        WaitHelper.setWaitTime(50, nextPageButton, driver);
    }
    //Return the result of the comparison
    public boolean getEqualsResult(String s){
        log.info("Execute 'getEqualsResult' method");
        Iterator<List<String>> itr = allResults.iterator();
        while(itr.hasNext()) {
            List<String> list = itr.next();
            Iterator<String> itrList = list.iterator();
            return getEqualsString(itrList, s);
        }
        return false;
    }
    //Find a particular string in a List <String>
    public boolean getEqualsString(Iterator<String> itr, String s) {
        while (itr.hasNext()){
            String element = itr.next();
            if(element.contains(s)) {
                return true;
            }
        }
        return false;
    }

    public String getConstant(String constant) {
        log.info("Get '" + constant + "' const");
        read = new ReadXMLFile();
        return read.getConstant(constant, PATH);
    }
}
