package com.kardashovdmitry;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Created by Dmitry on 28.09.2015.
 */
public class ReadXMLFile {
    private DocumentBuilderFactory dbf;
    private File file;
    private DocumentBuilder db;
    private Document doc;
    private String TAG_NAME = "taskdata";

    //The parser of an XML document and return the required constant
    public String getConstant(String elementName, String filePath){
        try{
            file = new File(filePath);
            dbf = DocumentBuilderFactory.newInstance();
            db = dbf.newDocumentBuilder();
            doc = db.parse(file);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName(TAG_NAME);
            for(int i=0; i<nList.getLength(); i++){
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element element = (Element) nNode;
                    return element.getElementsByTagName(elementName).item(i).getTextContent();
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }
}
