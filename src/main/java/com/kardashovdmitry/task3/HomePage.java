package com.kardashovdmitry.task3;

import com.kardashovdmitry.ReadXMLFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dmitry on 16.09.2015.
 */
public class HomePage {
    private WebDriver driver;
    private ReadXMLFile read;
    private String PATH = "src/main/resources/test3const.xml";
    By bookATrip = By.xpath("//section[@id='nav-widget-booking']");
    By flight = By.xpath("//a[@id='book-air-content-trigger']");
    By roundTripButton = By.xpath("//button[@id='roundTripBtn']");
    By fromField = By.xpath("//input[@id='originCity']");
    By toField = By.xpath("//input[@id='destinationCity']");
    By departDate = By.xpath("//input[@id='departureDate']");
    By returnDate = By.xpath("//input[@id='returnDate']");
    By exactDatesButton = By.xpath("//button[@id='exactDaysBtn']");
    By moneyButton = By.xpath("//button[@id='cashBtn']");
    By findFlightsButton = By.xpath("//button[@id='findFlightsSubmit']");
    private Logger log = LogManager.getLogger(HomePage.class);

    public HomePage(WebDriver driver) {
        log.info("Create a new HomePage object");
        this.driver = driver;
    }

    public HomePage clickElement(By locator) {
        log.info("Click element " + locator.toString());
        if(driver.findElement(locator).isEnabled()){

        } else {
            driver.findElement(locator).click();
        }
        return this;
    }

    public HomePage fillField(String s, By locator) {
        log.info("Fill '" + s + "' to" + locator.toString() + " field");
        driver.findElement(locator).sendKeys(s);
        return this;
    }

    public TicketsSelectionPage clickfindFlightsButton() {
        log.info("Click find Flights Button");
                driver.findElement(findFlightsButton).click();
        return new TicketsSelectionPage(driver);
    }
    //Generate the date of departure and return
    public String generateDate(String i){
        log.info("Execute 'generateDate' method");
        long anotherDate = Long.parseLong(i);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date currentDate = new Date();
        long time = currentDate.getTime();
        time = time + (60*60*24*1000*anotherDate);
        Date current = new Date(time);
        return sdf.format(current).toString();
    }

    public String getConstant(String constant) {
        log.info("Get '" + constant + "' const");
        read = new ReadXMLFile();
        return read.getConstant(constant, PATH);
    }

}
