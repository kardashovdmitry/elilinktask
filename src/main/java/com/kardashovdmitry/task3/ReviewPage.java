package com.kardashovdmitry.task3;

import com.kardashovdmitry.ReadXMLFile;
import com.kardashovdmitry.WaitHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Dmitry on 17.09.2015.
 */
public class ReviewPage {
    private WebDriver driver;
    private ReadXMLFile read;
    private String PATH = "src/main/resources/test3const.xml";
    By continueButton = By.xpath("//button[@id='continue_button']");
    private Logger log = LogManager.getLogger(ReviewPage.class);

    public ReviewPage(WebDriver driver) {
        log.info("Create a new ReviewPage object");
        this.driver = driver;
    }
    //Check whether the button is active
    public ReviewPage checkButtonIsActive(By locator){
        log.info("Execute 'checkButtonIsActive' method");
        if(driver.findElement(locator).isEnabled()) {
            System.out.println("Button '" + driver.findElement(locator).getText() + "' is active");
        } else {
            System.out.println("Button '" + driver.findElement(locator).getText() + "' is not active");
        }
        return this;
    }

    public String getConstant(String constant) {
        log.info("Get '" + constant + "' const");
        read = new ReadXMLFile();
        return read.getConstant(constant, PATH);
    }
}

