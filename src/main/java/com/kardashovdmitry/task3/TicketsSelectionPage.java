package com.kardashovdmitry.task3;

import com.kardashovdmitry.ReadXMLFile;
import com.kardashovdmitry.WaitHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Dmitry on 16.09.2015.
 */
public class TicketsSelectionPage {
    private WebDriver driver;
    private ReadXMLFile read;
    private String PATH = "src/main/resources/test3const.xml";
    By firstTicket = By.xpath("//button[@id='0_0_0']");
    By selectedTicket = By.xpath("//table[@class='flightDetailTable']");
    private Logger log = LogManager.getLogger(TicketsSelectionPage.class);

    public TicketsSelectionPage(WebDriver driver) {
        log.info("Create a new ReviewPage object");
        this.driver = driver;
    }
    //Select the first listed ticket
    public TicketsSelectionPage selectTicket() {
        log.info("Execute 'selectTicket' method");
        WaitHelper.setWaitTime(50, firstTicket, driver);
        driver.findElement(firstTicket).click();
        return this;
    }

    public TicketsSelectionPage waitSelectTicket(){
        log.info("Execute 'waitSelectTicket' method");
        WaitHelper.setWaitTime(50, selectedTicket, driver);
        return this;
    }

    public String getConstant(String constant) {
        log.info("Get '" + constant + "' const");
        read = new ReadXMLFile();
        return read.getConstant(constant, PATH);
    }

}
