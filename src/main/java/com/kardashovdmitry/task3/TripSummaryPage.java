package com.kardashovdmitry.task3;

import com.kardashovdmitry.ReadXMLFile;
import com.kardashovdmitry.WaitHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Dmitry on 16.09.2015.
 */
public class TripSummaryPage {
    private WebDriver driver;
    private ReadXMLFile read;
    private String PATH = "src/main/resources/test3const.xml";
    By continueButton = By.xpath("//button[@class='primaryInline rightCornerBtn floatRight']");
    private Logger log = LogManager.getLogger(PassengerInfoPage.class);

    public TripSummaryPage(WebDriver driver) {
        log.info("Create a new TripSummaryPage object");
        this.driver = driver;
    }

    public PassengerInfoPage clickElement(By locator) {
        log.info("Click element " + locator.toString());
        WaitHelper.setWaitTime(50, locator, driver);
        driver.findElement(locator).click();
        return new PassengerInfoPage(driver);
    }

    public String getConstant(String constant) {
        log.info("Get '" + constant + "' const");
        read = new ReadXMLFile();
        return read.getConstant(constant, PATH);
    }

}
