package com.kardashovdmitry.task3;

import com.kardashovdmitry.ReadXMLFile;
import com.kardashovdmitry.WaitHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Dmitry on 17.09.2015.
 */
public class PassengerInfoPage {
    private WebDriver driver;
    private ReadXMLFile read;
    private String PATH = "src/main/resources/test3const.xml";
    By firstName = By.xpath("//input[@id='firstName0']");
    By lastName = By.xpath("//input[@id='lastName0']");
    By gender = By.xpath("//select[@id='gender0']");
    By male = By.xpath("//option[@value='M']");
    By month = By.xpath("//select[@id='month0']");
    By day = By.xpath("//select[@id='day0']");
    By year = By.xpath("//select[@id='year0']");
    By testMonth = By.xpath("//option[@value='Jan']");
    By testDay = By.xpath("//option[@value='01']");
    By testYear = By.xpath("//option[@value='1994']");
    By emergencyFirstName = By.xpath("//input[@id='emgcFirstName_0']");
    By emergencyLastName = By.xpath("//input[@id='emgcLastName_0']");
    By emergencyPhoneNumber = By.xpath("//input[@id='emgcPhoneNumber_0']");
    By contactPhoneNumber = By.xpath("//input[@id='telephoneNumber0']");
    By contactEmail = By.xpath("//input[@id='email']");
    By confirmContactEmail = By.xpath("//input[@id='reEmail']");
    By continueButton = By.xpath("//button[@id='paxReviewPurchaseBtn']");
    private Logger log = LogManager.getLogger(PassengerInfoPage.class);



    public PassengerInfoPage(WebDriver driver) {
        log.info("Create a new PassengerInfoPage object");
        this.driver = driver;
    }

    public PassengerInfoPage clickElement(By locator) {
        log.info("Click element " + locator.toString());
        WaitHelper.setWaitTime(50, locator, driver);
        driver.findElement(locator).click();
        return this;
    }

    public PassengerInfoPage fillField(String s, By locator) {
        log.info("Fill '" + s + "' to" + locator.toString() + " field");
        WaitHelper.setWaitTime(50, locator, driver);
        driver.findElement(locator).sendKeys(s);
        return this;
    }

    public String getConstant(String constant) {
        log.info("Get '" + constant + "' const");
        read = new ReadXMLFile();
        return read.getConstant(constant, PATH);
    }

}
