# ElilinkTask

#Вводная информация
1. Разместите проект например в бесплатном репозитории bitbucket.org, так будет проще нам обмениваться результатами.
2. Для написания и запуска тестов используйте Intellij IDEA, JAVA 8, Selenium WebDriver, TestNG, используйте сборщик Maven или Gradle.
3. Для вывода логов в консоль и формирования лог файла используйте Log4J.
4. Вы должны овладеть PageObject pattern и показать ваш опыт в ООП.
5. Будет очень хорошо если ваши тесты будут Кроссбраузерными (FireFox, IE, Chrome), но по началу достаточно FireFox.
6. Все задания выполнить в одном проекте с возможностью запускать их кажое по отдельности и все вместе.


#Task 1
1. запустить браузер
2. открыть www.tut.by
3. В поле поиска написать automated testing 
4. Нажать найти 
5. Посчитать количество результатов на данной странице - написать в консоль 
6. Найти результат "Minsk Automated Testing Community"; 
7. если существует - перейти по ссылке, иначе - бросить org.openqa.selenium.WebDriverException


#Task 2
1. открыть в браузере почту гугл, залогиниться в нее с корректным аккаунтом.
2. перейти в разделы входящие, исходящие, спам ===> проверить что верные страницы открываются по этим переходам (некий ожидаемый header, к примеру) 
3. Произвести поиск в разделе входящие например по букве "a"
	===> сравнить ожидаемое количество найденных записей и актуальное ===> получить результаты поиска (для только первой страницы) в виде List<List<String>> 
4. Написать и отослать письмо кому-либо 
5. Вылогиниться из почты.


#Task 3
URL:https://www.delta.com/
Test the flight booking engine for flight combinations:

Flight combination # 1

- ROUND TRIP
- FROM: JFK (New York)
- TO: SVO (Moskow)
- DEPART DATE/RETURN DATE: pick any dates in the current month
- EXACT DATES
- SHOW PRICE IN: Money
- Leave the rest fields by default

Step 1 - Open Home page

Page is going to the next step

Step 2 - Home page | Booking widget

2.1-Go to 'Book a trip' booking widget
2.2-Click on 'FLIGHT' tab under 'Book a trip' booking widget 
2.3-Select Flight type (depending on Combinations table above) 
2.4-Enter Departure airport into 'FROM' field 
2.5-Enter Arrival airport into 'TO' field 
2.6-Select Departure/Arrival Date 
2.7-Select Date type (depending on Combinations table above) 
2.8-Select Price type (depending on Combinations table above) 
2.9-Click on 'Find Flights' button Page is going to the next step

Step 3 - Tickets selection page

3.1-Select the 1st ticket
3.2-Select the 2nd ticket
Page is going to the next step

Step 4 - Trip Summary page (step depends on input info)

4.1-Click on 'Continue' button
Page is going to the next step

Step 5 - Passenger Info page (step depends on input info)

5.1-Fill in all required Passenger Information fields 
5.2-Click on 'Continue' or 'Next: Trip Extras' button Page is going to the next step

Step 6 - Trip Extras page (step depends on input info)

6.1-Click on 'Review + Pay' button
Page is going to the next step

Step 7 - Credit Card Info page

7.1-Check, if button is active 'Complete Purchase'
